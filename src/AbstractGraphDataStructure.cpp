#include <AbstractGraphDataStructure.h>

bool AbstractGraphDataStructure::searchNode(AbstractNode* node)
{
    return (findNode(node) != nodesEnd());
}

bool AbstractGraphDataStructure::searchEdge(AbstractEdge* edge)
{
    return (findEdge(edge) != edgesEnd());
}

bool AbstractGraphDataStructure::searchEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    return (findEdge(src, dst, edge) != edgesEnd());
}

std::list< AbstractEdge* > AbstractGraphDataStructure::getIncomingEdges(AbstractNode* node)
{
    std::list< AbstractEdge* > result;
    
    for(auto& it = edgesBegin(); it != edgesEnd(); ++it)
    {
        auto edgeNodes = getEdgeNodes(it);
        if(edgeNodes.second == node)
        {
            result.push_back(*it);
        }
    }
    return result;
}

std::list< AbstractEdge* > AbstractGraphDataStructure::getOutgoingEdges(AbstractNode* node)
{
    std::list< AbstractEdge* > result;
    
    for(auto& it = edgesBegin(); it != edgesEnd(); ++it)
    {
        auto edgeNodes = getEdgeNodes(it);
        if(edgeNodes.first == node)
        {
            result.push_back(*it);
        }
    }
    return result;
}
