#include <gtest/gtest.h>
#include <ArrayGraphDataStructure.h>
#include <Node.h>
#include <Edge.h>

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_addNodeTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3;
    
    ds.addNode(&n1);
    ASSERT_EQ(ds.countNodes(), 1);
    
    ds.addNode(&n2);
    ASSERT_EQ(ds.countNodes(), 2);
    
    ds.addNode(&n3);
    ASSERT_EQ(ds.countNodes(), 3);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_addEdgeTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3;
    Edge e1, e2, e3;
    
    
    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    ds.addEdge(&n1, &n2, &e1);
    ASSERT_EQ(ds.countEdges(), 1);
    
    ds.addEdge(&n2, &n3, &e2);
    ASSERT_EQ(ds.countEdges(), 2);
    
    ds.addEdge(&n3, &n1, &e3);
    ASSERT_EQ(ds.countEdges(), 3);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_removeNodeTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3, n4;
    
    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    ds.removeNode(&n4);
    ASSERT_EQ(ds.countNodes(), 3);
    
    ds.removeNode(&n1);
    ASSERT_EQ(ds.countNodes(), 2);
    
    ds.removeNode(&n2);
    ASSERT_EQ(ds.countNodes(), 1);
    
    ds.removeNode(&n3);
    ASSERT_EQ(ds.countNodes(), 0);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_removeEdgeTest)
{
    ArrayGraphDataStructure ds;
    Node n1, n2, n3, n4;
    Edge e1, e2, e3, e4;

    ASSERT_EQ(ds.addNode(&n1), true);
    ASSERT_EQ(ds.addNode(&n2), true);
    ASSERT_EQ(ds.addNode(&n3), true);
    ASSERT_EQ(ds.addNode(&n1), false);
    
    ASSERT_EQ(ds.addEdge(&n1, &n2, &e1), true);
    ASSERT_EQ(ds.addEdge(&n1, &n2, &e1), false);
    ASSERT_EQ(ds.addEdge(&n1, &n3, &e1), true);
    ASSERT_EQ(ds.addEdge(&n2, &n3, &e2), true);
    ASSERT_EQ(ds.addEdge(&n3, &n1, &e3), true);

    ds.removeEdge(&e1);
    ASSERT_EQ(ds.countEdges(), 2);
    
    ds.removeEdge(&e2);
    ASSERT_EQ(ds.countEdges(), 1);
    
    ds.removeEdge(&e3);
    ASSERT_EQ(ds.countEdges(), 0);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_searchNodeTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3, n4;
    
    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    ASSERT_EQ(ds.searchNode(&n1), true);
    ASSERT_EQ(ds.searchNode(&n2), true);
    ASSERT_EQ(ds.searchNode(&n3), true);
    ASSERT_EQ(ds.searchNode(&n4), false);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_searchEdgeTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3;
    Edge e1, e2, e3, e4;

    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    ds.addEdge(&n1, &n2, &e1);
    ds.addEdge(&n2, &n3, &e2);
    ds.addEdge(&n3, &n1, &e3);
    
    ASSERT_EQ(ds.searchEdge(&e1), true);
    ASSERT_EQ(ds.searchEdge(&e2), true);
    ASSERT_EQ(ds.searchEdge(&e3), true);
    ASSERT_EQ(ds.searchEdge(&e4), false);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_getEdgeNodesTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3;
    Edge e1, e2, e3, e4;

    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    ds.addEdge(&n1, &n2, &e1);
    ds.addEdge(&n2, &n3, &e2);
    ds.addEdge(&n3, &n1, &e3);
    
    try {
        auto e4_nodes = ds.getEdgeNodes(&e4);
        FAIL();
    } catch ( const char* ) {
        
    }
    
    auto e1_nodes = ds.getEdgeNodes(&e1);
    ASSERT_EQ(e1_nodes.first, &n1);
    ASSERT_EQ(e1_nodes.second, &n2);
    
    auto e2_nodes = ds.getEdgeNodes(&e2);
    ASSERT_EQ(e2_nodes.first, &n2);
    ASSERT_EQ(e2_nodes.second, &n3);
    
    auto e3_nodes = ds.getEdgeNodes(&e3);
    ASSERT_EQ(e3_nodes.first, &n3);
    ASSERT_EQ(e3_nodes.second, &n1);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_NodeIteratorTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3;
    
    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    int count = 0;
    for(auto& it = ds.nodesBegin(); it != ds.nodesEnd(); ++it)
    {
        ++count;
    }
    ASSERT_EQ(count, 3);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_EdgeIteratorTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3;
    Edge e1, e2, e3;

    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    
    ds.addEdge(&n1, &n2, &e1);
    ds.addEdge(&n2, &n3, &e2);
    ds.addEdge(&n3, &n1, &e3);
    
    int count = 0;
    for(auto& it = ds.edgesBegin(); it != ds.edgesEnd(); ++it)
    {
        ++count;
    }
    ASSERT_EQ(count, 3);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_getIncomingEdgesTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3, n4;
    Edge e1, e2, e3, e4;

    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    ds.addNode(&n4);
    
    ds.addEdge(&n1, &n2, &e1);
    ds.addEdge(&n2, &n3, &e2);
    ds.addEdge(&n3, &n1, &e3);
    ds.addEdge(&n4, &n1, &e4);
    auto edges_n1 = ds.getIncomingEdges(&n1);
    auto edges_n2 = ds.getIncomingEdges(&n2);
    auto edges_n3 = ds.getIncomingEdges(&n3);
    auto edges_n4 = ds.getIncomingEdges(&n4);
    
    ASSERT_EQ(edges_n1.front(), &e3);
    edges_n1.pop_front();
    ASSERT_EQ(edges_n1.front(), &e4);
    ASSERT_EQ(edges_n2.front(), &e1);
    ASSERT_EQ(edges_n3.front(), &e2);
    ASSERT_EQ(edges_n4.size(), 0);
}

TEST(ArrayGraphDataStructureTestCase, ArrayGraphDataStructure_getOutgoingEdgesTest)
{
    ArrayGraphDataStructure ds;
    Node n1,n2,n3, n4;
    Edge e1, e2, e3, e4;

    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addNode(&n3);
    ds.addNode(&n4);
    
    ds.addEdge(&n1, &n2, &e1);
    ds.addEdge(&n1, &n4, &e4);
    
    ds.addEdge(&n2, &n3, &e2);
    ds.addEdge(&n3, &n1, &e3);
    
    auto edges_n1 = ds.getOutgoingEdges(&n1);
    auto edges_n2 = ds.getOutgoingEdges(&n2);
    auto edges_n3 = ds.getOutgoingEdges(&n3);
    auto edges_n4 = ds.getOutgoingEdges(&n4);
    
    ASSERT_EQ(edges_n1.front(), &e4);
    edges_n1.pop_front();
    ASSERT_EQ(edges_n1.front(), &e1);
    ASSERT_EQ(edges_n2.front(), &e2);
    ASSERT_EQ(edges_n3.front(), &e3);
    ASSERT_EQ(edges_n4.size(), 0);
}