#include <gtest/gtest.h>
#include <Graph.h>
#include <ListGraphDataStructure.h>
#include <Edge.h>
#include <Node.h>

TEST(GraphTestCase, Graph_addEdgeTest0)
{
    ListGraphDataStructure ds;
    Graph g(&ds);
    Node n1, n2;
    Edge e1, e2;
    
    ASSERT_EQ(g.addNode(&n1), true);
    ASSERT_EQ(g.addNode(&n2), true);
    ASSERT_EQ(g.countNodes(), 2);
    ASSERT_EQ(g.addEdge(&n1, &n2, &e1), true);
    ASSERT_EQ(g.countEdges(), 1);
    ASSERT_EQ(g.addEdge(&n1, &n2, &e2), false);
    ASSERT_EQ(g.countEdges(), 1);
}

TEST(GraphTestCase, Graph_setModelTest0)
{
    ListGraphDataStructure ds;
    Graph g;
    
    Node n1, n2;
    Edge e1, e2;
    
    ds.addNode(&n1);
    ds.addNode(&n2);
    ds.addEdge(&n1, &n2, &e1);
    ds.addEdge(&n1, &n2, &e2);
    ASSERT_EQ(g.setModel(&ds), false);
    ds.removeEdge(&e2);
    ASSERT_EQ(g.setModel(&ds), true);
}