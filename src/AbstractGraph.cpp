#include <AbstractGraph.h>

bool AbstractGraph::addNode(AbstractNode* node)
{
    if(checkModel())
    {
        return m_model->addNode(node);
    }
    throw "model is not set";
}

bool AbstractGraph::addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->addEdge(src, dst, edge);
    }
    throw "model is not set";
}

bool AbstractGraph::removeNode(AbstractNode* node)
{
    if(checkModel())
    {
        return m_model->removeNode(node);
    }
    throw "model is not set";
}

bool AbstractGraph::removeEdge(AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->removeEdge(edge);
    }
    throw "model is not set";
}

bool AbstractGraph::searchNode(AbstractNode* node)
{
    if(checkModel())
    {
        return m_model->searchNode(node);
    }
    throw "model is not set";
}

bool AbstractGraph::searchEdge(AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->searchEdge(edge);
    }
    throw "model is not set";
}

bool AbstractGraph::searchEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->searchEdge(src, dst, edge);
    }
    throw "model is not set";
}

size_t AbstractGraph::countEdges() const
{
    if(checkModel())
    {
        return m_model->countEdges();
    }
    throw "model is not set";
}

size_t AbstractGraph::countNodes() const
{
    if(checkModel())
    {
        return m_model->countNodes();
    }
    throw "model is not set";
}

AbstractEdgeIterator& AbstractGraph::edgesBegin()
{
    if(checkModel())
    {
        return m_model->edgesBegin();
    }
    throw "model is not set";
}

AbstractEdgeIterator& AbstractGraph::edgesEnd()
{
    if(checkModel())
    {
        return m_model->edgesEnd();
    }
    throw "model is not set";
}

AbstractEdgeIterator& AbstractGraph::findEdge(AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->findEdge(edge);
    }
    throw "model is not set";
}

AbstractEdgeIterator& AbstractGraph::findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->findEdge(src, dst, edge);
    }
    throw "model is not set";
}


AbstractNodeIterator& AbstractGraph::findNode(AbstractNode* node)
{
    if(checkModel())
    {
        return m_model->findNode(node);
    }
    throw "model is not set";
}

std::pair< AbstractNode*, AbstractNode* > AbstractGraph::getEdgeNodes(AbstractEdge* edge)
{
    if(checkModel())
    {
        return m_model->getEdgeNodes(edge);
    }
    throw "model is not set";
}

std::pair< AbstractNode*, AbstractNode* > AbstractGraph::getEdgeNodes(AbstractEdgeIterator& edgeIterator)
{
    if(checkModel())
    {
        return m_model->getEdgeNodes(edgeIterator);
    }
    throw "model is not set";
    
}

std::list< AbstractEdge* > AbstractGraph::getIncomingEdges(AbstractNode* node)
{
    if(checkModel())
    {
        return m_model->getIncomingEdges(node);
    }
    throw "model is not set";
}

std::list< AbstractEdge* > AbstractGraph::getOutgoingEdges(AbstractNode* node)
{
    if(checkModel())
    {
        return m_model->getOutgoingEdges(node);
    }
    throw "model is not set";
}

AbstractNodeIterator& AbstractGraph::nodesBegin()
{
    if(checkModel())
    {
        return m_model->nodesBegin();
    }
    throw "model is not set";
}

AbstractNodeIterator& AbstractGraph::nodesEnd()
{
    if(checkModel())
    {
        return m_model->nodesEnd();
    }
    throw "model is not set";
}
