/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */
#include <algorithm>
#include <ListGraphDataStructure.h>

ListGraphDataStructure::NodeIterator::NodeIterator(const NodeContainer::iterator nodePos)
    :   m_nodePos(nodePos)
{ }

ListGraphDataStructure::NodeIterator::NodeIterator(const NodeIterator& other)
{
    m_nodePos = other.m_nodePos;
}

ListGraphDataStructure::NodeIterator& ListGraphDataStructure::NodeIterator::operator=(
    const ListGraphDataStructure::NodeIterator& other)
{
    if(&other != this)
    {
        m_nodePos = other.m_nodePos;
    }
    return *this;
}

ListGraphDataStructure::NodeIterator& ListGraphDataStructure::NodeIterator::operator=(const AbstractNodeIterator& other)
{
    auto& it = dynamic_cast<const NodeIterator&>(other);
    return *this = it;
}

AbstractNodeIterator& ListGraphDataStructure::NodeIterator::operator++()
{
    ++m_nodePos;
    return *this;
}

bool ListGraphDataStructure::NodeIterator::operator==(const AbstractNodeIterator& rhs)
{
    return dynamic_cast<const NodeIterator&>(rhs).m_nodePos == m_nodePos;
}


bool ListGraphDataStructure::NodeIterator::operator!=(const AbstractNodeIterator& rhs)
{
    return dynamic_cast<const NodeIterator&>(rhs).m_nodePos != m_nodePos;
}

AbstractNode* ListGraphDataStructure::NodeIterator::operator*()
{
    return *m_nodePos;
}

ListGraphDataStructure::EdgeIterator::EdgeIterator(const EdgeContainer::iterator edgePos)
    :   m_edgePos(edgePos)
{ }

ListGraphDataStructure::EdgeIterator::EdgeIterator(const EdgeIterator& other)
{
    m_edgePos = other.m_edgePos;
}

ListGraphDataStructure::EdgeIterator& ListGraphDataStructure::EdgeIterator::operator=(
    const ListGraphDataStructure::EdgeIterator& other)
{
    if(&other != this)
    {
        m_edgePos = other.m_edgePos;
    }
    return *this;
}

ListGraphDataStructure::EdgeIterator& ListGraphDataStructure::EdgeIterator::operator=(
    const AbstractEdgeIterator& other)
{
    auto& it = dynamic_cast<const EdgeIterator&>(other);
    return *this = it;
}

AbstractEdgeIterator& ListGraphDataStructure::EdgeIterator::operator++()
{
    ++m_edgePos;
    return *this;
}

bool ListGraphDataStructure::EdgeIterator::operator==(const AbstractEdgeIterator& rhs)
{
    return dynamic_cast<const EdgeIterator&>(rhs).m_edgePos == m_edgePos;
}

bool ListGraphDataStructure::EdgeIterator::operator!=(const AbstractEdgeIterator& rhs)
{
    return dynamic_cast<const EdgeIterator&>(rhs).m_edgePos != m_edgePos;
}

AbstractEdge* ListGraphDataStructure::EdgeIterator::operator*()
{
    return std::get<2>(*m_edgePos);
}

ListGraphDataStructure::~ListGraphDataStructure()
{
    // to do
}

bool ListGraphDataStructure::addNode(AbstractNode* node)
{
    if(std::find(m_nodes.begin(), m_nodes.end(), node) == m_nodes.end())
    {
        m_nodes.push_back(node);
        return true;
    }
    return false;
}

bool ListGraphDataStructure::addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    if(std::find(m_nodes.begin(), m_nodes.end(), src) == m_nodes.end() || 
       std::find(m_nodes.begin(), m_nodes.end(), dst) == m_nodes.end())
    {
        return false;
    }
    if(std::find(m_edges.begin(), m_edges.end(), std::make_tuple<>(src, dst, edge)) == m_edges.end())
    {
        m_edges.push_back(std::make_tuple<>(src, dst, edge));
        return true;
    }
    return false;
}

bool ListGraphDataStructure::removeNode(AbstractNode* node)
{
    auto node_it = std::find(m_nodes.begin(), m_nodes.end(), node);
    if(node_it == m_nodes.end())
    {
        return false;
    }
    m_nodes.erase(node_it);
    
    auto edge_it = m_edges.begin();
    while(edge_it != m_edges.end())
    {
        if( (std::get<0>(*edge_it) == node) || (std::get<1>(*edge_it) == node) )
        {
            edge_it = m_edges.erase(edge_it);
        } else {
            ++edge_it;
        }
    }
    return true;
}

bool ListGraphDataStructure::removeEdge(AbstractEdge* edge)
{
    bool result = false;
    
    auto it = m_edges.begin();
    while(it != m_edges.end())
    {
        if( std::get<2>(*it) == edge )
        {
            it = m_edges.erase(it);
            result = true;
        } else {
            ++it;
        }
    }
    return result;
}

AbstractNodeIterator& ListGraphDataStructure::findNode(AbstractNode* node)
{
    for(auto& it = nodesBegin(); it != nodesEnd(); ++it)
    {
        if(*it == node)
        {
            m_nodeFindResult = it;
            return m_nodeFindResult;
        }
    }
    m_nodeFindResult = nodesEnd();
    return m_nodeFindResult;
}

AbstractEdgeIterator& ListGraphDataStructure::findEdge(AbstractEdge* edge)
{
    for(auto& it = edgesBegin(); it != edgesEnd(); ++it)
    {
        if(*it == edge)
        {
            m_edgeFindResult = it;
            return m_edgeFindResult;
        }
    }
    m_edgeFindResult = edgesEnd();
    return m_edgeFindResult;
}

AbstractEdgeIterator& ListGraphDataStructure::findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    for(auto& it = edgesBegin(); it != edgesEnd(); ++it)
    {
        if( (*it == edge) && (getEdgeNodes(it) == std::make_pair<>(src, dst)) )
        {
            m_edgeFindResult = it;
            return m_edgeFindResult;
        }
    }
    m_edgeFindResult = edgesEnd();
    return m_edgeFindResult;
}

std::pair< AbstractNode*, AbstractNode* > ListGraphDataStructure::getEdgeNodes(AbstractEdge* edge)
{
    for(auto it = m_edges.begin(); it != m_edges.end(); ++it)
    {
        if( std::get<2>(*it) == edge )
        {
            return std::pair<AbstractNode*, AbstractNode*>(std::get<0>(*it), std::get<1>(*it));
        }
    }
    
    throw "no such edge in graph";
}

std::pair< AbstractNode*, AbstractNode* > ListGraphDataStructure::getEdgeNodes(AbstractEdgeIterator& edgeIterator)
{
    EdgeIterator it = dynamic_cast<EdgeIterator&>(edgeIterator);
    auto edgePos = it.getEdgePos();
    return std::pair< AbstractNode*, AbstractNode* >(std::get<0>(*edgePos), std::get<1>(*edgePos));
}


size_t ListGraphDataStructure::countNodes() const
{
    return m_nodes.size();
}

size_t ListGraphDataStructure::countEdges() const
{
    return m_edges.size();
}

AbstractNodeIterator& ListGraphDataStructure::nodesBegin()
{
    m_nodesBegin = NodeIterator(m_nodes.begin());
    return m_nodesBegin;
}

AbstractNodeIterator& ListGraphDataStructure::nodesEnd()
{
    m_nodesEnd = NodeIterator(m_nodes.end());
    return m_nodesEnd;
}

AbstractEdgeIterator& ListGraphDataStructure::edgesBegin()
{
    m_edgesBegin = EdgeIterator(m_edges.begin());
    return m_edgesBegin;
}

AbstractEdgeIterator& ListGraphDataStructure::edgesEnd()
{
    m_edgesEnd = EdgeIterator(m_edges.end());
    return m_edgesEnd;
}


