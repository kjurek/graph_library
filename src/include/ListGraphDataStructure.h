/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef LISTGRAPHDATASTRUCTURE_H
#define LISTGRAPHDATASTRUCTURE_H

#include <list>
#include <tuple>

#include <AbstractGraphDataStructure.h>

class ListGraphDataStructure : public AbstractGraphDataStructure
{
    typedef std::list<AbstractNode*> NodeContainer;
    typedef std::list<std::tuple<AbstractNode*, AbstractNode*, AbstractEdge*> > EdgeContainer;
public:
    class NodeIterator : public AbstractNodeIterator
    {
    public:
        NodeIterator() { }
        NodeIterator(const NodeContainer::iterator nodePos);
        NodeIterator(const NodeIterator& other);
        NodeIterator& operator=(const NodeIterator& other);
        NodeIterator& operator=(const AbstractNodeIterator& other);
        virtual AbstractNodeIterator& operator++();
        virtual bool operator==(const AbstractNodeIterator& rhs);
        virtual bool operator!=(const AbstractNodeIterator& rhs);
        virtual AbstractNode* operator*();
        NodeContainer::iterator getNodePos() const { return m_nodePos; }
    protected:
        NodeContainer::iterator m_nodePos;
    };
    
    class EdgeIterator : public AbstractEdgeIterator
    {
    public:
        EdgeIterator() { }
        EdgeIterator(const EdgeContainer::iterator edgePos);
        EdgeIterator(const EdgeIterator& other);
        EdgeIterator& operator=(const EdgeIterator& other);
        EdgeIterator& operator=(const AbstractEdgeIterator& other);
        virtual AbstractEdgeIterator& operator++();
        virtual bool operator==(const AbstractEdgeIterator& rhs);
        virtual bool operator!=(const AbstractEdgeIterator& rhs);
        virtual AbstractEdge* operator*();
        EdgeContainer::iterator getEdgePos() const { return m_edgePos; }
    protected:
        EdgeContainer::iterator m_edgePos;
    };

    virtual ~ListGraphDataStructure();
    
    virtual bool addNode(AbstractNode* node);
    virtual bool addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual bool removeNode(AbstractNode* node);
    virtual bool removeEdge(AbstractEdge* edge);
    
    virtual AbstractNodeIterator& findNode(AbstractNode* node);
    virtual AbstractEdgeIterator& findEdge(AbstractEdge* edge);
    virtual AbstractEdgeIterator& findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual size_t countNodes() const;
    virtual size_t countEdges() const;
    
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdge* edge);
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdgeIterator& edgeIterator);

    virtual AbstractNodeIterator& nodesBegin();
    virtual AbstractEdgeIterator& edgesBegin();
    virtual AbstractNodeIterator& nodesEnd();
    virtual AbstractEdgeIterator& edgesEnd();
protected:
    NodeContainer m_nodes;
    EdgeContainer m_edges;
private:
    NodeIterator m_nodesBegin;
    NodeIterator m_nodesEnd;
    EdgeIterator m_edgesBegin;
    EdgeIterator m_edgesEnd;
    NodeIterator m_nodeFindResult;
    EdgeIterator m_edgeFindResult;
};

#endif // LISTGRAPHDATASTRUCTURE_H
