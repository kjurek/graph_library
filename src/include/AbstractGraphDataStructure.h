
/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef ABSTRACTGRAPHDATASTRUCTURE_H
#define ABSTRACTGRAPHDATASTRUCTURE_H

#include <utility>
#include <cstdlib>
#include <list>

#include <AbstractNode.h>
#include <AbstractEdge.h>
#include <AbstractGraphIterator.h>

class AbstractGraphDataStructure
{
public:   
    virtual bool addNode(AbstractNode* node) = 0;
    virtual bool addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge) = 0;
    
    virtual bool removeNode(AbstractNode* node) = 0;
    virtual bool removeEdge(AbstractEdge* edge) = 0;
    
    virtual size_t countNodes() const = 0;
    virtual size_t countEdges() const = 0;
    
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdge* edge) = 0;
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdgeIterator& edgeIterator) = 0;
    
    virtual AbstractNodeIterator& nodesBegin() = 0;
    virtual AbstractEdgeIterator& edgesBegin() = 0;
    virtual AbstractNodeIterator& nodesEnd() = 0;
    virtual AbstractEdgeIterator& edgesEnd() = 0;
    
    virtual AbstractNodeIterator& findNode(AbstractNode* node) = 0;
    virtual AbstractEdgeIterator& findEdge(AbstractEdge* edge) = 0;
    virtual AbstractEdgeIterator& findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge) = 0;
    
    virtual bool searchNode(AbstractNode* node);
    virtual bool searchEdge(AbstractEdge* edge);
    virtual bool searchEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual std::list<AbstractEdge*> getIncomingEdges(AbstractNode* node);
    virtual std::list<AbstractEdge*> getOutgoingEdges(AbstractNode* node);
};

#endif // ABSTRACTGRAPHDATASTRUCTURE_H
