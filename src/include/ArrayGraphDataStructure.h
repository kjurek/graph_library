/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef ARRAYGRAPHDATASTRUCTURE_H
#define ARRAYGRAPHDATASTRUCTURE_H

#include <unordered_map>
#include <list>
#include <cstdlib>

#include <AbstractGraphDataStructure.h>
#include <AbstractNode.h>

class ArrayGraphDataStructure : public AbstractGraphDataStructure
{
    template <typename T, typename U>
    class pairHashFunction {
    public:
        std::size_t operator()(const std::pair<T,U>& p) const 
        { 
            return std::hash<T>()(p.first) ^ std::hash<U>()(p.second); 
        }
    };
    typedef std::list<AbstractNode*> NodeContainer;
    typedef std::list<AbstractEdge*> EdgeList;
    typedef std::unordered_map< 
        std::pair<AbstractNode*, AbstractNode*>,
        std::list<AbstractEdge*>,
        pairHashFunction<AbstractNode*, AbstractNode*> > EdgeContainer;
public:
    class NodeIterator : public AbstractNodeIterator
    {
    public:
        NodeIterator() { }
        NodeIterator(NodeContainer::iterator nodePos);
        NodeIterator(const NodeIterator& other);
        NodeIterator& operator=(const NodeIterator& other);
        NodeIterator& operator=(const AbstractNodeIterator& other);
        virtual AbstractNodeIterator& operator++();
        virtual bool operator==(const AbstractNodeIterator& rhs);
        virtual bool operator!=(const AbstractNodeIterator& rhs);
        virtual AbstractNode* operator*();
        NodeContainer::iterator getNodePos() const { return m_nodePos; }
    protected:
        NodeContainer::iterator m_nodePos;
    };
    
    class EdgeIterator : public AbstractEdgeIterator
    {
    public:
        EdgeIterator() { }
        EdgeIterator(ArrayGraphDataStructure* ds, EdgeContainer::iterator edgePos, const EdgeList::iterator edgeListPos);
        EdgeIterator(const EdgeIterator& other);
        EdgeIterator& operator=(const EdgeIterator& other);
        EdgeIterator& operator=(const AbstractEdgeIterator& other);
        virtual AbstractEdgeIterator& operator++();
        virtual bool operator==(const AbstractEdgeIterator& rhs);
        virtual bool operator!=(const AbstractEdgeIterator& rhs);
        virtual AbstractEdge* operator*();
        EdgeContainer::iterator getEdgePos() const { return m_edgePos; }
        EdgeList::iterator getEdgeListPos() const { return m_edgeListPos; }
    protected:
        ArrayGraphDataStructure* m_ds;
        EdgeContainer::iterator m_edgePos;
        EdgeList::iterator m_edgeListPos;
    };
    
    virtual ~ArrayGraphDataStructure();
    
    virtual bool addNode(AbstractNode* node);
    virtual bool addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual bool removeNode(AbstractNode* node);
    virtual bool removeEdge(AbstractEdge* edge);
    
    virtual size_t countNodes() const;
    virtual size_t countEdges() const;
    
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdge* edge);
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdgeIterator& edgeIterator);
    
    virtual AbstractNodeIterator& findNode(AbstractNode* node);
    virtual AbstractEdgeIterator& findEdge(AbstractEdge* edge);
    virtual AbstractEdgeIterator& findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual AbstractNodeIterator& nodesBegin();
    virtual AbstractEdgeIterator& edgesBegin();
    virtual AbstractNodeIterator& nodesEnd();
    virtual AbstractEdgeIterator& edgesEnd();
protected:
    NodeContainer m_nodes;
    EdgeContainer m_edges;
private:
    NodeIterator m_nodesBegin;
    NodeIterator m_nodesEnd;
    EdgeIterator m_edgesBegin;
    EdgeIterator m_edgesEnd;
    NodeIterator m_nodeFindResult;
    EdgeIterator m_edgeFindResult;
};

#endif // ARRAYGRAPHDATASTRUCTURE_H
