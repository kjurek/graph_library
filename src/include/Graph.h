/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef GRAPH_H
#define GRAPH_H

#include <AbstractGraph.h>

class Graph : public AbstractGraph
{
public:
    Graph() { };
    Graph(AbstractGraphDataStructure* model) : AbstractGraph(model) { }
    virtual ~Graph() { }
    virtual bool setModel(AbstractGraphDataStructure* model);
    virtual bool addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
};

#endif // GRAPH_H
