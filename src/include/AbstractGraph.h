/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef ABSTRACTGRAPH_H
#define ABSTRACTGRAPH_H

#include <AbstractGraphDataStructure.h>
#include <AbstractNode.h>
#include <AbstractEdge.h>

class AbstractGraph
{
public:
    AbstractGraph() : m_model(nullptr) { }
    AbstractGraph(AbstractGraphDataStructure* model) { setModel(model); }
    virtual ~AbstractGraph() { };
    virtual bool setModel(AbstractGraphDataStructure* model) { m_model = model; return true; };
    virtual AbstractGraphDataStructure* getModel() { return m_model; }
    
    virtual bool addNode(AbstractNode* node);
    virtual bool addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual bool removeNode(AbstractNode* node);
    virtual bool removeEdge(AbstractEdge* edge);
    
    virtual size_t countNodes() const;
    virtual size_t countEdges() const;
    
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdge* edge);
    virtual std::pair<AbstractNode*, AbstractNode*> getEdgeNodes(AbstractEdgeIterator& edgeIterator);
    
    virtual AbstractNodeIterator& nodesBegin();
    virtual AbstractEdgeIterator& edgesBegin();
    virtual AbstractNodeIterator& nodesEnd();
    virtual AbstractEdgeIterator& edgesEnd();
    
    virtual AbstractNodeIterator& findNode(AbstractNode* node);
    virtual AbstractEdgeIterator& findEdge(AbstractEdge* edge);
    virtual AbstractEdgeIterator& findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual bool searchNode(AbstractNode* node);
    virtual bool searchEdge(AbstractEdge* edge);
    virtual bool searchEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge);
    
    virtual std::list<AbstractEdge*> getIncomingEdges(AbstractNode* node);
    virtual std::list<AbstractEdge*> getOutgoingEdges(AbstractNode* node);
protected:
    AbstractGraphDataStructure* m_model;
private:
    bool checkModel() const { return nullptr != m_model; }
};

#endif // ABSTRACTGRAPH_H
