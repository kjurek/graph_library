/*
 * Copyright 2014 <copyright holder> <email>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#include <algorithm>
#include <ArrayGraphDataStructure.h>

ArrayGraphDataStructure::NodeIterator::NodeIterator(NodeContainer::iterator nodePos)
    :   m_nodePos(nodePos)
{ }

ArrayGraphDataStructure::NodeIterator::NodeIterator(const NodeIterator& other)
{
    m_nodePos = other.m_nodePos;
}

ArrayGraphDataStructure::NodeIterator& ArrayGraphDataStructure::NodeIterator::operator=(
    const ArrayGraphDataStructure::NodeIterator& other)
{
    if(&other != this)
    {
        m_nodePos = other.m_nodePos;
    }
    return *this;
}

ArrayGraphDataStructure::NodeIterator& ArrayGraphDataStructure::NodeIterator::operator=(
    const AbstractNodeIterator& other)
{
    auto& it = dynamic_cast<const NodeIterator&>(other);
    return *this = it;
}

AbstractNodeIterator& ArrayGraphDataStructure::NodeIterator::operator++()
{
    ++m_nodePos;
    return *this;
}

bool ArrayGraphDataStructure::NodeIterator::operator==(const AbstractNodeIterator& rhs)
{
    return dynamic_cast<const NodeIterator&>(rhs).m_nodePos == m_nodePos;
}

bool ArrayGraphDataStructure::NodeIterator::operator!=(const AbstractNodeIterator& rhs)
{
    return dynamic_cast<const NodeIterator&>(rhs).m_nodePos != m_nodePos;
}

AbstractNode* ArrayGraphDataStructure::NodeIterator::operator*()
{
    return *m_nodePos;
}

ArrayGraphDataStructure::EdgeIterator::EdgeIterator(ArrayGraphDataStructure* ds,
                                                    const EdgeContainer::iterator edgePos, 
                                                    EdgeList::iterator edgeListPos)
    :   m_ds(ds), m_edgePos(edgePos), m_edgeListPos(edgeListPos)
{ }

ArrayGraphDataStructure::EdgeIterator::EdgeIterator(const ArrayGraphDataStructure::EdgeIterator& other)
    :   m_ds(other.m_ds), m_edgePos(other.m_edgePos), m_edgeListPos(other.m_edgeListPos)
{ }

ArrayGraphDataStructure::EdgeIterator& ArrayGraphDataStructure::EdgeIterator::operator=(
    const ArrayGraphDataStructure::EdgeIterator& other)
{
    if(&other != this)
    {
        m_edgeListPos = other.m_edgeListPos;
        m_edgePos = other.m_edgePos;
    }
    return *this;
}

ArrayGraphDataStructure::EdgeIterator& ArrayGraphDataStructure::EdgeIterator::operator=(
    const AbstractEdgeIterator& other)
{
    auto& it = dynamic_cast<const EdgeIterator&>(other);
    return *this = it;
}

AbstractEdgeIterator& ArrayGraphDataStructure::EdgeIterator::operator++()
{
    ++m_edgeListPos;
    if(m_edgeListPos == m_edgePos->second.end())
    {
        ++m_edgePos;
        if(m_edgePos == m_ds->m_edges.end())
        {
            m_edgeListPos = EdgeList::iterator();
        } else {
            m_edgeListPos = m_edgePos->second.begin();
        }
    }
    return *this;
}

bool ArrayGraphDataStructure::EdgeIterator::operator==(const AbstractEdgeIterator& rhs)
{
    const EdgeIterator& other = dynamic_cast<const EdgeIterator&>(rhs);
    if( (m_edgePos == m_ds->m_edges.end()) || (other.m_edgePos == m_ds->m_edges.end()) )
    {
        return other.m_edgePos == m_edgePos;
    }
    return (other.m_edgeListPos == m_edgeListPos) && (other.m_edgePos == m_edgePos);
}

bool ArrayGraphDataStructure::EdgeIterator::operator!=(const AbstractEdgeIterator& rhs)
{
    return !(*this == rhs);
}

AbstractEdge* ArrayGraphDataStructure::EdgeIterator::operator*()
{
    return *m_edgeListPos;
}

ArrayGraphDataStructure::~ArrayGraphDataStructure()
{
    // to do
}

bool ArrayGraphDataStructure::addNode(AbstractNode* node)
{
    if(std::find(m_nodes.begin(), m_nodes.end(), node) == m_nodes.end())
    {
        m_nodes.push_back(node);
        return true;
    }
    return false;
}

bool ArrayGraphDataStructure::addEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    if(std::find(m_nodes.begin(), m_nodes.end(), src) == m_nodes.end() || 
       std::find(m_nodes.begin(), m_nodes.end(), dst) == m_nodes.end())
    {
        return false;
    }
    auto& edgeList = m_edges[std::make_pair(src, dst)];
    if(std::find(edgeList.begin(), edgeList.end(), edge) == edgeList.end())
    {
        m_edges[std::make_pair(src, dst)].push_back(edge);
        return true;
    }
    return false;
}

bool ArrayGraphDataStructure::removeNode(AbstractNode* node)
{
    auto node_it = std::find(m_nodes.begin(), m_nodes.end(), node);
    if(node_it == m_nodes.end())
    {
        return false;
    }
    m_nodes.erase(node_it);
    
    auto edge_it = m_edges.begin();
    while(edge_it != m_edges.end())
    {
        auto nodePair = edge_it->first;
        if(nodePair.first == node || nodePair.second == node)
        {
            edge_it = m_edges.erase(edge_it);
        } else {
            ++edge_it;
        }
    }
    return true;
}

bool ArrayGraphDataStructure::removeEdge(AbstractEdge* edge)
{
    bool result = false;
    auto it = m_edges.begin();
    
    while(it != m_edges.end())
    {
        auto edge_it = std::find(it->second.begin(), it->second.end(), edge);
        if(edge_it != it->second.end())
        {
            it->second.erase(edge_it);
            result = true;
        }
        if(it->second.size() == 0)
        {
            it = m_edges.erase(it);
        } else {
            ++it;
        }
    }
    return result;
}

size_t ArrayGraphDataStructure::countNodes() const
{
    return m_nodes.size();
}

size_t ArrayGraphDataStructure::countEdges() const
{
    size_t count = 0;
    for(auto it = m_edges.begin(); it != m_edges.end(); ++it)
    {
        count += it->second.size();
    }
    return count;
}

std::pair< AbstractNode*, AbstractNode* > ArrayGraphDataStructure::getEdgeNodes(AbstractEdge* edge)
{
    for(auto it = m_edges.begin(); it != m_edges.end(); ++it)
    {
        auto edge_it = std::find(it->second.begin(), it->second.end(), edge);
        
        if(edge_it != it->second.end())
        {
            return it->first;
        }
    }
    
    throw "no such edge in graph";
}

std::pair< AbstractNode*, AbstractNode* > ArrayGraphDataStructure::getEdgeNodes(AbstractEdgeIterator& edgeIterator)
{
    auto& it = dynamic_cast<EdgeIterator&>(edgeIterator);
    return it.getEdgePos()->first;
}

AbstractNodeIterator& ArrayGraphDataStructure::findNode(AbstractNode* node)
{
    for(auto& it = nodesBegin(); it != nodesEnd(); ++it)
    {
        if(*it == node)
        {
            m_nodeFindResult = it;
            return m_nodeFindResult;
        }
    }
    m_nodeFindResult = nodesEnd();
    return m_nodeFindResult;
}

AbstractEdgeIterator& ArrayGraphDataStructure::findEdge(AbstractEdge* edge)
{
    for(auto& it = edgesBegin(); it != edgesEnd(); ++it)
    {
        if(*it == edge)
        {
            m_edgeFindResult = it;
            return m_edgeFindResult;
        }
    }
    m_edgeFindResult = edgesEnd();
    return m_edgeFindResult;
}

AbstractEdgeIterator& ArrayGraphDataStructure::findEdge(AbstractNode* src, AbstractNode* dst, AbstractEdge* edge)
{
    for(auto& it = edgesBegin(); it != edgesEnd(); ++it)
    {
        if( (*it == edge) && (getEdgeNodes(it) == std::make_pair<>(src, dst)) )
        {
            m_edgeFindResult = it;
            return m_edgeFindResult;
        }
    }
    m_edgeFindResult = edgesEnd();
    return m_edgeFindResult;
}

AbstractNodeIterator& ArrayGraphDataStructure::nodesBegin()
{
    m_nodesBegin = NodeIterator(m_nodes.begin());
    return m_nodesBegin;
}

AbstractNodeIterator& ArrayGraphDataStructure::nodesEnd()
{
    m_nodesEnd = NodeIterator(m_nodes.end());
    return m_nodesEnd;
}

AbstractEdgeIterator& ArrayGraphDataStructure::edgesBegin()
{
    if(m_edges.size() == 0)
    {
        m_edgesBegin = EdgeIterator(this, m_edges.end(), EdgeList::iterator());
    } else {
        m_edgesBegin = EdgeIterator(this, m_edges.begin(), m_edges.begin()->second.begin());
    }
    return m_edgesBegin;
}

AbstractEdgeIterator& ArrayGraphDataStructure::edgesEnd()
{
    m_edgesEnd = EdgeIterator(this, m_edges.end(), EdgeList::iterator());
    return m_edgesEnd;
}
